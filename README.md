# YouDwldr

The Python3 script for downloading videos from Youtube (Works on linux)

# Requirements:
## linux
## python3
### python modules:
- requests
- requests-html
- os
- sys
- urllib
- json
## utils
- ffmpeg
# USING:
use command :
> python3 dwldr.py args...
## args: 
- -h - help
- -r (link to video) - download video 
- -l (file name) - download videos from list in file. file format: one link - one line
- -qn - show quality names
- -q - Required quality (default hd720)

## Example
> python3 ./dwld.py -l ./test_list.txt -q medium

script will create two dirs in curent dir "./tmp" and "./out". Videos will saved to the "out" dir.
