

#!/usr/bin/ python3
# coding: utf-8

#import modules
import requests
import os
import sys
import urllib.parse
from urllib.parse import unquote
from urllib.parse import unquote_plus
from urllib.request import urlopen, Request
import json 


addres = "https://www.youtube.com/"

qualitys = ["tiny", "small", "medium", "hd720", "hd1080", "hd2160"]

def_format = { "quality" : "hd720", "mimeType":"video/mp4"}

help_message = ''' 
-r <href to video>	 
-l <file name with list of videos for downloading>
-q <quality of video> 
-qn - to see list of quality values
'''
try :
	os.mkdir("./tmp")
	
except FileExistsError as e:
	pass

try :
	os.mkdir("./out")
	
except FileExistsError as e:
	pass




def get_v_id_watch(reff):
	pos = reff.find("?v=")
	if(pos == -1) :
		raise Exception("Wrong video reff. Can't find video id:" + reff)
	pos += 3
	return str(reff[pos:pos+11])


def get_v_id_shared(reff):
	pos = reff.find(".be/")
	if(pos == -1) :
		raise Exception("Wrong video reff. Can't find video id:" + reff)
	pos += 4
	return str(reff[pos:pos+11])

 
def get_v_id(reff):
	if( not (reff.find("youtube.com/watch") == -1) ):
		return get_v_id_watch(reff)
	
	if( not (reff.find("youtu.be/") == -1) ) :
		return get_v_id_shared(reff)

	raise Exception("Wrong video reference:" + reff)


def get_vid_inf(id):
	method = "get_video_info?video_id="
	full_ref = addres + method + id
	
	r = Request(full_ref)
	resp = urlopen(r)

	data = str(resp.read())
	#print(data)
	return data


def get_uniqe_key(dic, key):
	res = key 
	num = 1
	while True :
		temp = res
		if not (temp in dic) :
			#print("break")
			return key
		num = num + 1
		res = res + num
	print("oldkey:" + key + ";",  "newkey: " + res)
	return res


def parse_info(info):
	params = info.split('&')

	res = {}
	for k in params :
		temp = k.split('=');
		#sometimes receives strange data
		temp[0] = temp[0].replace("b'", "")
		temp[0] = temp[0].replace("'", "")
		temp[1] = temp[1].replace("b'", "")
		temp[1] = temp[1].replace("'", "")
		res[temp[0]] = unquote_plus(temp[1]);
	return res;


def handle_no_args():
	print("\033[31m Error: Needs args. Use -h argument for help\033[0m")
	exit(1)

def get_arg_value(args, arg_name):
	try :
		index = args.index(arg_name)
	except Exception as e :
		return (False, "")

	if(len(args) < index + 2):
		raise Exception("Error: Can't find value of arg:" + arg_name)

	return (True, args[index+1])

arg_names_with_values = ["-r", "-l", "-q"]
arg_names_without_values = ["-h", "-qn"]

def parse_args():
	args = sys.argv
	if(len(args) < 2):
		handle_no_args()
	res = {}
	for name in arg_names_with_values : 
		v = get_arg_value(args, name)
		if(v[0]):
			res[name] = v[1]

	for name in arg_names_without_values : 
		v = 0
		try :
			v = args.index(name)
		except :
			continue

		res[name] = True
	return res
		
def clear_line(n):
	for i in range(0, n):
		print("\b", end='')

def download_file(url, file_name):
	#from https://stackoverflow.com/questions/16694907/download-large-file-in-python-with-requests
	# I want use the curl command :)
	# NOTE the stream=True parameter below
	progress = 0
	progress_str = "  " + str(progress) + " bytes"
	print("loading: " + file_name)
	with requests.get(url, stream=True) as r:
		r.raise_for_status()
		with open(file_name, 'wb') as f:
			for chunk in r.iter_content(chunk_size=8192): 
				if chunk: # filter out keep-alive new chunks
					progress += len(chunk)
					f.write(chunk)
					clear_line(len(progress_str))
					progress_str = "  " + str(progress) + " bytes"
					print(progress_str, end='')
					# f.flush()
			f.close()
	print()
	print("\033[32m******OK******\033[0m")
	return file_name


def down_load_and_mix(urls, file_name):
	print("downloading video stream:")
	download_file(urls[0], "./tmp/tvid.mp4")
	print("downloading audio stream:")
	download_file(urls[1], "./tmp/taud.mp4")

	print("mixing...")



	cmd = "ffmpeg -y -i ./tmp/tvid.mp4 -i ./tmp/taud.mp4 -c copy " + "./tmp/" + file_name + ".mp4"
	os.system(cmd)
	cmd = "mv " + "./tmp/" + file_name + ".mp4" + " ./out/ \n rm ./tmp/*"
	os.system(cmd)
	print("Done to \"./out/" +  file_name + ".mp4")

					#video input file	 audio input file


def get_download_urls(formats, adaptive_formats, quality):
	for v_format in formats:
		m_type = v_format["mimeType"]

		if not ("url" in v_format):
			continue

		if(m_type.find("video/mp4;") == -1):
			continue

		if(v_format["quality"] != quality):
			continue

		return (v_format["url"], None)
	if(qualitys.index(quality) <= qualitys.index("hd720")) :
		return (None, None)
	audio = ""
	#find audio track
	for ad_format in adaptive_formats:
		if(ad_format["mimeType"].find("audio/mp4;") != -1):
			if not ("url" in ad_format) :
				continue
			audio = ad_format["url"]
			break
		audio = "notfound"

	for ad_format in adaptive_formats:
		m_type = ad_format["mimeType"]

		if not ("url" in ad_format):
			continue

		if(m_type.find("video/mp4;") == -1):
			continue

		if(ad_format["quality"] != quality):
			continue

		return (ad_format["url"], audio)
	return (None, None)

def convert_title(title):
	# Костылинг
	spec_sym = ["\\", "/", " ", "-"]
	temp = title
	for sym in spec_sym :
		temp = temp.replace(sym, "\\ ")
	
	return temp

def down_load_video(reff, quality):
	vid_info = get_vid_inf(get_v_id(reff))

	parsed_info = parse_info(vid_info)

	#print(json.dumps(parsed_info, indent=4))

	if not ("status" in parsed_info) :
		print("\033[31mError: Undefined: " + json.dumps(parsed_info, indent=4))
		return	

	if(not parsed_info["status"] == "ok"):
		print(json.dumps(parsed_info, indent=4))
		print("\033[31mError: When downloading video: " + reff + "\n " + parsed_info["reason"] + "\033[0m")
		return;

	pl_resp = {}

	try :
		pl_resp = json.loads(parsed_info["player_response"])
	except Exception as e :
		print("\033[31mError: Parse Error: " + reff + "\n " + json.dumps(parsed_info, indent=4) + "\033[0m")
		return

	# print(get_v_id_watch(link))
	# print(get_v_id_shared(sh_link))
	#print (json.dumps(pl_resp, indent=4))
	# print(json.dumps(adaptive_formats, indent=4))
	# print(json.dumps(formats, indent=4))
	# print(title)
	#print (str(bs_temp, "utf-8"))
	pl_status =  pl_resp["playabilityStatus"]
	if(not pl_status["status"] == "OK"):

		print("\033[31mError: When downloading video: " + reff + "\n reason: " + pl_status["reason"].encode("unicode_escape").decode("unicode_escape") + "\033[0m")
		#print(json.dumps(parsed_info, indent=4))
		return;
	

	formats = pl_resp["streamingData"]["formats"]

	adaptive_formats = pl_resp["streamingData"]["adaptiveFormats"]


	title = pl_resp["videoDetails"]["title"].encode("unicode_escape").decode("unicode_escape")	

	title = convert_title(title)

	print("info:OK\n downloading: \033[33m" + title + "\033[0m")

	urls = get_download_urls(formats, adaptive_formats, quality)

	if((urls[0] == None) and (urls[1] == None)):
		print("\033[31mError: When downloading video: " + title + "\033[0m" + " source not found in quality: " + quality+ "(may be ciphered source//TODO )")
		return
	
	#print(json.dumps(adaptive_formats, indent=4))
	
	#print(json.dumps(formats, indent=4))

	if(urls[1] == "notfound"):
		print("\033[31mError: When downloading video: " + title + "\033[0m" + " Audio source not found: " + quality+ "(may be ciphered source//TODO )")
		return

	if((urls[0] != None) and (urls[1] != None)):
		down_load_and_mix(urls, title)
		return

	download_file(urls[0], "./out/" + title + ".mp4")


def print_q_names():
	for name in qualitys:
		print(name)

def load_list(fname):
	fd = open(fname, "r")
	lines = fd.readlines()
	for i in range(0, len(lines)):
		temp = lines[i]
		if temp.find("\n") > -1 :
			temp = temp[0:-1]
			lines[i] = temp

	fd.close()
	return lines

def print_vlist(vlist):
	print("\033[32mList of videos for downloading:\033[33m")
	for item in vlist:
		print("  " + item)
	print("\033[0m")

def download_list(v_list, quality):
	for item in v_list:
		down_load_video(item, quality)

def main():
	params = parse_args()
	video_list = []
	
	is_ref = "-r"
	is_list = "-l"
	is_qa = "-q"

	is_qn = "-qn"
	is_help = "-h"

	quality_val = "hd720" #default 

	if(is_qn in params):
		print_q_names()
		exit(0)

	if(is_help in params):
		print(help_message)
		exit(0)


	if not ((is_ref in params) or (is_list in params)):
		print("\033[31m Error! Need one of arguments : -r or -l use -h for help")
		exit(1)

	if is_ref in params :
		video_list.append(params[is_ref])


	if is_list in params :
		video_list += load_list(params[is_list])

	if is_qa in params :
		index = -1
		try :
			index = qualitys.index(params[is_qa])
		except Exception as e :
			print("\033[31m Error! Wrong quality value : use -qn to see available values")
			exit(1)
		quality_val = qualitys[index]

	print_vlist(video_list)
	print("\033[32mselected quality(default hd720): \033[33m" + quality_val + "\033[0m")

	download_list(video_list, quality_val)

main()




